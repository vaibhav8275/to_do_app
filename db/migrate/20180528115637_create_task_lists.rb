class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.string :title
      t.text :description
      t.references :project, foreign_key: true
      t.references :created_by, foreign_key: true
      t.integer :status, default: 1

      t.timestamps
    end
  end
end
