# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.create! :email => 'vaibhav@gmail.com', :password => 'abcd1234', :password_confirmation => 'abcd1234'
user.add_role :admin

user2 = User.create! :email => 'admin@gmail.com', :password => 'abcd1234', :password_confirmation => 'abcd1234'
user2.add_role :admin

user3 = User.create! :email => 'john@gmail.com', :password => 'abcd1234', :password_confirmation => 'abcd1234'
user3.add_role :developer

user4 = User.create! :email => 'simon@gmail.com', :password => 'abcd1234', :password_confirmation => 'abcd1234'
user4.add_role :developer


(1..40).each do |v|
	project = Project.create! :name => "Project #{v}",:created_by => (rand(1..2) == 1 ? user : user2), :description => 'Lorem Lipsum Lorem Lipsum Lorem Lipsum Lorem Lipsum Lorem Lipsum Lorem Lipsum Lorem Lipsum Lorem Lipsum Lorem.'
	project_dev = ([user, user2, user3, user4].sample)
	(1..30).each do |i|
		project_id = rand(1...11)
		task=Task.create! :title => "Task #{i}", :description => "Lorem Lipsum Task #{i} for #{project.name}", :created_by => (rand(1..2) == 1 ? user : user2), :project => project,:status => (Task.statuses.keys)[rand(0..2)]
		project_dev.add_role :developer, task
	end
end