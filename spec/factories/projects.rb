FactoryBot.define do
  factory :project do
    name "Project 1"
    description "Lorem Lipsum"
    created_by nil
    # association :created_by, factory: :admin
  end

  factory :invalid_project, class: Project do
    description "Invalid Project Lorem Lipsum"
  end

	# factory :admin_with_project do
	# 	after(:create) do |project|
	# 	  project.created_by = admin
	# 	end
	# end

	# factory :project_with_task do
 #    after(:create) do |project|
 #      create(:project_task, project: project,created_by: nil)
 #    end
	# end
end
