FactoryBot.define do

  factory :user do
    email 'example@example.com'
    password 'please'
    password_confirmation 'please'
    # association :projects, factory: :project

    factory :admin do
        after(:create) {|user| user.add_role(:admin)}
    end

    factory :developer do
        after(:create) {|user| user.add_role(:developer)}
    end

    factory :vaibhav, aliases: [:created_by] do
        after(:create) {|user| user.add_role(:admin)}
    end

		# factory :user_with_project do
		#     after(:create) do |user|
		#     	user.add_role(:admin)
		#       create(:project, created_by: user)
		#     end
		# end

  end



  # factory :developer, class: User do
  #   name { "vaibhav" }
  #   email "vaibhav@gmail.com"
  #   password "abcd@1234"
  # end

end
