FactoryBot.define do
  factory :task do
    title "Task 1"
    description "Task desc"
    project FactoryBot.build(:project)
    # created_by FactoryBot.build(:admin)
    # build(:created_by)
    status 'on_hold'
  end

  factory :invalid_task, class: Task do
    description "Invalid Invalid Invalid"
  end
end
