require 'rails_helper'

RSpec.describe Project, type: :model do
	let(:created_by_admin){ build(:admin)}
  let(:project) { build(:project,:created_by => created_by_admin) }
  let(:invalid_project) { build(:invalid_project) }

  it "is valid with valid attributes" do
    expect(project).to be_valid
  end

  it "is not valid without a created_by" do
    expect(invalid_project).to_not be_valid
  end

  it "is not valid without a name" do
    expect(invalid_project).to_not be_valid
  end

	it "on create increase project count" do
    expect { project.save }.to change(Project, :count).by(1)
	end
end
