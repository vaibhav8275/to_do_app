require 'rails_helper'

RSpec.describe Task, type: :model do
	let(:created_by_admin){ build(:admin)}
  let(:project) { build(:project,:created_by => created_by_admin) }
  let(:task) { build(:task,:created_by => created_by_admin) }
  let(:invalid_task) { build(:invalid_task) }

  it "is valid with valid attributes" do
    expect(task).to be_valid
  end

  it "is not valid without a created_by" do
    expect(invalid_task).to_not be_valid
  end

  it "is not valid without a title" do
    expect(invalid_task).to_not be_valid
  end

	it "on create increase task count" do
    expect { task.save }.to change(Task, :count).by(1)
	end
end
