require 'rails_helper'
require "cancan/matchers"
RSpec.describe Ability, type: :model do
	let(:user) { FactoryBot.create(:admin) }
	let(:project) { FactoryBot.create(:project,:created_by => user) }
	let(:task) { FactoryBot.create(:task,:created_by => user,:project => project ) }

	context "for a user with admin role" do
		subject(:ability) { described_class.new(user) }	
		it{ should be_able_to(:manage, project) }
		it{ should be_able_to(:manage, task) }
	end	

	context "for a user with developer role" do
		let(:user) { FactoryBot.create(:developer) }
		subject(:ability) { described_class.new(user) }

		it{ should be_able_to(:read, project) }
		it{ should_not be_able_to(:create, project) }
		it{ should_not be_able_to(:update, project) }
		it{ should_not be_able_to(:delete, project) }

		it{ should be_able_to(:read, task) }
		it{ should be_able_to(:create, task) }
		it{ should be_able_to(:update, task) }
		it{ should_not be_able_to(:delete, task) }		
	end	
end