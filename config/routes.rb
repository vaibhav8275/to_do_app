Rails.application.routes.draw do
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  authenticated do  	
  	resources :projects do 
	   	resources :tasks do
	   		get 'assign_task/:id', to: 'tasks#assign_task', as: :assign_task
	   		get 'remove_task/:id', to: 'tasks#assign_task', as: :remove_task
	   	end
   	end
  end

  devise_for :users
  unauthenticated do
		devise_scope :user do
		  root to: "devise/sessions#new"
		end
  end
  root to: "projects#index"

end
