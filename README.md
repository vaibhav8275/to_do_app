# README
========
bundle install
rails db:migrate
rails db:seed

Default Role
============
1.) By default developer role assigned on signup
2.) admin credentials
user : admin
pwd : abcd1234
3.) developer credentials
user : john@gmail.com
pwd : abcd1234

Further Improvement Scope
=========================
1.) We can add comment section to Tasks.
2.) Searching & Filtering.
3.) Remove uneccessary routes.
4.) Remove uneccessary templates created with scaffold.

