class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource

  # GET /projects
  # GET /projects.json
  def index
    if current_user.has_role?(:admin)
      @projects = Project.order(id: :desc).paginate(:page => params[:page], :per_page => 10)
    elsif current_user.has_role?(:developer)
      @tasks = Task.with_role(:developer, current_user)
      @projects = Project.where('id in (?)',@tasks.pluck(:project_id).to_a).order(id: :desc).paginate(:page => params[:page], :per_page => 10)
    end
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
    @project_tasks = @project.tasks.order(id: :desc)
    @on_hold_tasks = @project_tasks.on_hold
    @in_progress_tasks = @project_tasks.in_progress
    @completed_tasks = @project_tasks.completed
  end

  # GET /projects/new
  def new
    @project = Project.new
  end

  # GET /projects/1/edit
  def edit
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = current_user.projects.build(project_params)

    respond_to do |format|
      if @project.save
        format.html { redirect_to @project, notice: 'Project was successfully created.' }
        format.json { render :show, status: :created, location: @project }
        format.js {}
      else
        format.html { render :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
        format.js {}
      end
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { render :show, status: :ok, location: @project }
        format.js {}
      else
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
        format.js {}
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.js {}
      format.html { redirect_to projects_url, notice: 'Project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(:name, :description, :created_by_id)
    end
end
