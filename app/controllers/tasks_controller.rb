class TasksController < ApplicationController
  before_action :set_developer, only: [:assign_task,:update,:create]
  before_action :set_task, only: [:show, :edit, :update, :destroy, :assign_task]
  
  load_and_authorize_resource

  # GET /tasks
  # GET /tasks.json
  def index
    if current_user.has_role(:admin)
      @tasks = Task.all
    elsif current_user.has_role(:developer)
      @tasks = Task.with_role(:developer, current_user)
    end
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
  end

  # GET /tasks/new
  def new
    @task = current_user.tasks.build(:project_id => params[:project_id])
  end

  # GET /tasks/1/edit
  def edit
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @task = current_user.tasks.build(task_params)
    @task.project_id = params[:project_id]
    respond_to do |format|
      if @task.save
        assign_task
        format.html { redirect_to @task, notice: 'Task was successfully created.' }
        format.json { render :show, status: :created, location: @task }
        format.js {}
      else
        format.html { render :new }
        format.json { render json: @task.errors, status: :unprocessable_entity }
        format.js {}
      end
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update
      # puts "============checking params from update"
      # puts task_params.inspect
    respond_to do |format|
      if @task.update(task_params)
        assign_task
        format.html { redirect_to @task, notice: 'Task was successfully updated.' }
        format.json { render :show, status: :ok, location: @task }
        format.js {}
      else
        format.html { render :edit }
        format.json { render json: @task.errors, status: :unprocessable_entity }
        format.js {}
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    # byebug
    @task.destroy
    respond_to do |format|
      format.js {}
      format.html { redirect_to project_tasks_url(@task.project), notice: 'Task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = Task.find(params[:id])
    end

    def set_developer
      # byebug
      @developer=User.find_by(:id => task_params[:developer_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.require(:task).permit(:id,:title, :description, :project_id,:developer_id, :created_by_id, :status)
    end

    def assign_task
      @task.assign_task(@developer)
    end

    # def remove_task
    #   @task.remove_task(@developer)
    # end
end
