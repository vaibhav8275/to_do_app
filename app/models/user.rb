class User < ApplicationRecord
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :projects, :class_name => 'Project', :foreign_key => 'created_by_id'
  has_many :tasks, :class_name => 'Task', :foreign_key => 'created_by_id'

  after_create :set_default_role
  # or 
  # before_validation :set_default_role 

  private
  def set_default_role
    self.add_role :developer
  end
end
