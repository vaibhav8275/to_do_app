class Project < ApplicationRecord
  belongs_to :created_by, :class_name => 'User', :foreign_key => 'created_by_id'
  has_many :tasks, dependent: :destroy

  validates :name, presence: true
  validates :created_by, presence: true
end
