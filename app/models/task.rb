class Task < ApplicationRecord
	resourcify
	enum status: { on_hold: 1, in_progress: 2, completed: 3 }
  belongs_to :project
  belongs_to :created_by, :class_name => 'User', :foreign_key => 'created_by_id'
  attr_accessor :developer_id

  validates :created_by, presence: true
  validates :project, presence: true
  validates :title, presence: true

  def assign_task(user)
  	remove_task
  	user.add_role :developer, self
  end

  def remove_task
  	# user.remove_role :developer, self 
    self.roles.where(:name => 'developer').delete_all if self.roles.pluck(:name).include? ('developer')
  end

  def developer
    self.roles.first.users.first if !self.roles.blank?
  end
end
